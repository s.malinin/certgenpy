import sys
import json
import os
import shutil
from PyQt5 import QtWidgets, uic
from openpyxl.reader.excel import load_workbook
from ui.main import Ui_MainWindow
from libs.common import get_xls_sheets, xls_max_row, save_json_to_file, generate_certificate_pptx, log_timestamp

class MainForm(QtWidgets.QMainWindow, Ui_MainWindow):
    
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent=parent)
        self.setupUi(self)
        self.setupActions()
        self.setupData()
    
    def setupActions(self):

        # Menus
        self.actMenuMainNewProject.triggered.connect(self.menu_main_new)
        self.actMenuMainOpenProject.triggered.connect(self.menu_main_open)
        self.actMenuMainSaveProject.triggered.connect(self.menu_main_save)
        self.actMenuMainSaveAs.triggered.connect(self.menu_main_save_as)
        self.actMenuMainExit.triggered.connect(self.menu_main_exit)

        # Buttons
        self.btnAwardListOpen.clicked.connect(self.btn_award_list_open)
        self.btnTemplateCertOpen.clicked.connect(self.btn_template_cert_open)
        self.btnTemplateDplOpen.clicked.connect(self.btn_template_dpl_open)
        self.btnCreate.clicked.connect(self.btn_create)

        # Combo boxes
        self.cbAwardListSheet.currentIndexChanged.connect(self.cb_award_list_sheet_changed)
        self.cbTextAlign.currentIndexChanged.connect(self.cb_text_align_changed)

    def setupData(self):
        self.project_file = ""
        self.project_folder = ""
        self.project_settings = {}
        self.gen_ready_state = [False, False, False]
        self.award_list_filename = "Список.xlsx"
        self.cert_filename = "Шаблон-Сертификат.pptx"
        self.dpl_filename = "Шаблон-Диплом.pptx"
        self.text_state_choosen = "Выбран"
        self.text_state_nofile = "Не выбран"
        self.color_default = "black"
        self.color_choosen = "green"

    def cleanAllForms(self):
        self.setWindowTitle("Генератор сертификатов")
        self.setupData()
        self.lbl_set_text_color(self.lblAwardListState, self.text_state_nofile, self.color_default)
        self.lbl_set_text_color(self.lblTemplateCertState, self.text_state_nofile, self.color_default)
        self.lbl_set_text_color(self.lblTemplateDplState, self.text_state_nofile, self.color_default)
        self.cbAwardListSheet.clear()
        self.cbTextAlign.setCurrentText("Лево")
        self.btn_gen_active()
        self.statusBar().clearMessage()
    
    def menu_main_new(self):
        project_name, success = QtWidgets.QInputDialog.getText(self, "Новый проект", "Название проекта: ")
        if success:
            self.cleanAllForms()
            current_dir = os.path.dirname(__file__)
            self.project_file = os.path.join(current_dir, project_name, project_name + ".cgp")
            self.project_folder = os.path.join(current_dir, project_name)
            try:
                os.mkdir(self.project_folder)
                self.project_settings["xlsx_columns"] = self.edt_columns_to_list()
                self.project_settings["coords"] = self.edt_coords_to_list()
                self.project_settings["project_name"] = project_name
                self.project_settings["align"] = self.cbTextAlign.currentText()
                save_json_to_file(self.project_settings, self.project_file)
                self.statusBar().showMessage("Создан новый проект.")
                self.setWindowTitle(self.windowTitle() + " - " + project_name)
                self.actMenuMainSaveProject.setEnabled(True)
            except:
                self.statusBar().showMessage("Ошибка при создании проекта.")
        else:
            self.statusBar().showMessage("Создание проекта отменено.")

    def menu_main_open(self, pfile=None):
        
        if pfile:
            success = True
        else:
            pfile, success = QtWidgets.QFileDialog.getOpenFileName(self, "Открыть проект", "", "Проекты (*.cgp)")
        
        if success:

            self.cleanAllForms()
            self.project_file = pfile
            
            with open(self.project_file, "r") as pf:
                self.project_settings = json.load(pf)
            self.project_folder = os.path.dirname(self.project_file)

            self.setWindowTitle(self.windowTitle() + " - " + self.project_settings["project_name"])
            
            self.edt_settings_to_columns()
            self.edt_settings_to_coords()

            self.cbTextAlign.setCurrentText(self.project_settings.get("align", "Лево"))

            award_list_file = os.path.join(self.project_folder, self.award_list_filename)
            if os.path.isfile(award_list_file):
                sheet_from_settings = self.project_settings["sheet"]
                self.award_list = load_workbook(award_list_file)
                self.cb_award_enlist_sheet(self.award_list)
                self.gen_ready_state[0] = True
                self.cbAwardListSheet.setCurrentText(sheet_from_settings)

            cert_file = os.path.join(self.project_folder, self.cert_filename)
            if os.path.isfile(cert_file):
                self.lbl_set_text_color(self.lblTemplateCertState, self.text_state_choosen, self.color_choosen)
                self.gen_ready_state[1] = True

            dpl_file = os.path.join(self.project_folder, self.dpl_filename)
            if os.path.isfile(dpl_file):
                self.lbl_set_text_color(self.lblTemplateDplState, self.text_state_choosen, self.color_choosen)
                self.gen_ready_state[2] = True

            self.actMenuMainSaveProject.setEnabled(True)
            self.btn_gen_active()
            self.statusBar().showMessage("Проект успешно открыт")
        else:
            self.statusBar().showMessage("Открытие проекта отменено.")

    def menu_main_save(self):
        self.project_settings["xlsx_columns"] = self.edt_columns_to_list()
        self.project_settings["coords"] = self.edt_coords_to_list()
        save_json_to_file(self.project_settings, self.project_file)

    def menu_main_save_as(self):
        project_name, success = QtWidgets.QInputDialog.getText(self, "Сохранить как", "Название проекта:")
        if success:
            current_dir = os.path.dirname(__file__)
            project_file = os.path.join(current_dir, project_name, project_name + ".cgp")
            project_folder = os.path.join(current_dir, project_name)
            settings = self.project_settings
            try:
                os.mkdir(project_folder)
                settings["project_name"] = project_name
                save_json_to_file(settings, project_file)
                for file in [self.award_list_filename, self.cert_filename, self.dpl_filename]:
                    source = os.path.join(self.project_folder, file)
                    dest = os.path.join(project_folder, file)
                    if os.path.isfile(source):
                        shutil.copyfile(source, dest)
                self.statusBar().showMessage("Проект скопирован")
                self.menu_main_open(project_file)
            except:
                self.statusBar().showMessage("Ошибка при копировании проекта.")
        else:
            self.statusBar().showMessage("Копирование проекта отменено.")

    def menu_main_exit(self):
        QtWidgets.qApp.exit()

    def btn_award_list_open(self):
        award_file, success = QtWidgets.QFileDialog.getOpenFileName(self, "Список награждаемых", "", "Файлы Excel (*.xls*);; Все файлы (*)")
        if success:
            p_award_list = os.path.join(self.project_folder, self.award_list_filename)
            try:
                shutil.copyfile(award_file, p_award_list)
            except shutil.SameFileError:
                pass
            self.award_list = load_workbook(p_award_list)
            self.cb_award_enlist_sheet(self.award_list)
            self.statusBar().showMessage("Список скопирован в проект.")
            self.gen_ready_state[0] = True
            self.btn_gen_active()
        else:
            self.statusBar().showMessage("Открытие списка отменено.")
    
    def btn_template_cert_open(self):
        cert_file, success = QtWidgets.QFileDialog.getOpenFileName(self, "Шаблон сертификата", "", "Файлы Powerpoint (*.ppt*);; Все файлы (*)")
        if success:
            p_cert_file = os.path.join(self.project_folder, self.cert_filename)
            try:
                shutil.copyfile(cert_file, p_cert_file)
            except shutil.SameFileError:
                pass
            self.lbl_set_text_color(self.lblTemplateCertState, self.text_state_choosen, self.color_choosen)
            self.statusBar().showMessage("Шаблон сертификата скопирован в проект.")
            self.gen_ready_state[1] = True
            self.btn_gen_active()
        else:
            self.statusBar().showMessage("Открытие шаблона отменено.")

    def btn_template_dpl_open(self):
        dip_file, success = QtWidgets.QFileDialog.getOpenFileName(self, "Шаблон диплома", "", "Файлы Powerpoint (*.ppt*);; Все файлы (*)")
        if success:
            p_dip_file = os.path.join(self.project_folder, self.dpl_filename)
            try:
                shutil.copyfile(dip_file, p_dip_file)
            except shutil.SameFileError:
                pass
            self.lbl_set_text_color(self.lblTemplateDplState, self.text_state_choosen, self.color_choosen)
            self.statusBar().showMessage("Шаблон диплома скопирован в проект.")
            self.gen_ready_state[2] = True
            self.btn_gen_active()
        else:
            self.statusBar().showMessage("Открытие шаблона отменено.")

    def btn_gen_active(self):
        if sum(self.gen_ready_state) == 3:
            self.btnCreate.setEnabled(True)
        else:
            self.btnCreate.setEnabled(False)

    def btn_create(self):
        cert_path = os.path.join(self.project_folder, "Сертификаты")
        dpl_path = os.path.join(self.project_folder, "Дипломы")
        for path in (cert_path, dpl_path):
            if not os.path.isdir(path):
                os.mkdir(path)
        
        self.tedtLog.append("{} - Запуск генератора.".format(log_timestamp()))

        cert_counter = 0
        gen_range_low = int(self.edtGenRangeLow.text())
        gen_range_high = int(self.edtGenRangeHigh.text())
        gen_sheet = self.cbAwardListSheet.currentText()
        gen_cols = self.edt_columns_to_list()
        gen_coords = self.edt_coords_to_list()
        gen_align = self.cbTextAlign.currentText()

        for row in range(gen_range_low, gen_range_high + 1):
            progress = (gen_range_low + row) * 100 / (gen_range_high + 1)
            self.pbCreate.setValue(int(progress))
            state, error_filename = generate_certificate_pptx(self.project_folder, self.award_list[gen_sheet], row, gen_cols, gen_coords, gen_align)            
            if state:
                cert_counter += 1
            else:
                self.tedtLog.append("{} - Ошибка: {} не создан.".format(log_timestamp(), error_filename))
            
        
        self.tedtLog.append("{} - Завершено: {} файлов создано.".format(log_timestamp(), cert_counter))

    def cb_award_enlist_sheet(self, list_excel):
        sheets = get_xls_sheets(list_excel)
        self.cbAwardListSheet.clear()
        for sheet in sheets:
            self.cbAwardListSheet.addItem(sheet)
            self.cbAwardListSheet.setEnabled(True)
            self.lbl_set_text_color(self.lblAwardListState, self.text_state_choosen, self.color_choosen)

    def cb_award_list_sheet_changed(self):
        self.project_settings["sheet"] = self.cbAwardListSheet.currentText()
        if self.project_settings["sheet"]:
            self.gen_max_row = xls_max_row(self.award_list, 
                self.project_settings["sheet"], 
                self.project_settings["xlsx_columns"][0])
            self.edtGenRangeHigh.setText(str(self.gen_max_row))
            self.edtGenRangeLow.setText("2")
        else:
            self.edtGenRangeHigh.clear()
            self.edtGenRangeLow.clear()

    def cb_text_align_changed(self):
        self.project_settings["align"] = self.cbTextAlign.currentText()

    def lbl_set_text_color(self, label, text, color):
        label.setText(text)
        label.setStyleSheet("QLabel { color: %s; }" % color)
    
    def edt_columns_to_list(self):
        xlsx_columns = []
        xlsx_columns.append(self.edtColumnsName.text())
        xlsx_columns.append(self.edtColumnsSchool.text())
        xlsx_columns.append(self.edtColumnsCity.text())
        xlsx_columns.append(self.edtColumnsPlace.text())
        return xlsx_columns

    def edt_settings_to_columns(self):
        xlsx_columns = self.project_settings["xlsx_columns"]
        self.edtColumnsName.setText(xlsx_columns[0])
        self.edtColumnsSchool.setText(xlsx_columns[1])
        self.edtColumnsCity.setText(xlsx_columns[2])
        self.edtColumnsPlace.setText(xlsx_columns[3])
    
    def edt_coords_to_list(self):
        coords = []
        coords.append(self.edtCoordsNamePlace.text())
        coords.append(self.edtCoordsSchool.text())
        coords.append(self.edtCoordsCity.text())
        return coords

    def edt_settings_to_coords(self):
        coords = self.project_settings["coords"]
        self.edtCoordsNamePlace.setText(coords[0])
        self.edtCoordsSchool.setText(coords[1])
        self.edtCoordsCity.setText(coords[2])

if __name__ == "__main__":
    
    app = QtWidgets.QApplication([])
    main_form = MainForm()
    main_form.show()

    sys.exit(app.exec())
