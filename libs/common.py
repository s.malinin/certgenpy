import json
import os
import re
from datetime import datetime
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.dml import MSO_THEME_COLOR
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE, PP_ALIGN
from pptx.util import Cm, Pt
from openpyxl import load_workbook


def xls_max_row(xlsx_file, xlsx_sheet_name, target_column):
    xlsx_sheet = xlsx_file[xlsx_sheet_name]
    for row in range(xlsx_sheet.max_row, 1, -1):
        cell_value = xlsx_sheet.cell(row, int(target_column)).value
        if cell_value is not None:
            return row
    return False


def get_xls_sheets(list_award):
    sheet_list = [sheet for sheet in list_award.sheetnames]
    return sheet_list


def save_json_to_file(object, filename):
    try:
        with open(filename, "w") as of:
            json.dump(object, of)
        return True
    except:
        return False


def clean_cell(value):
    """
    Function cleans some stuff from string like extra spaces at ends and tabs.
    :param value: string source to clean
    :return: cleaner string or source value if it wasn't string.
    """
    if isinstance(value, str):
        value = value.strip()
        value = value.replace("\t", "")
    
    return value


def log_timestamp():
    datetime_now = datetime.now()
    return datetime_now.strftime("%H:%M:%S")


def align_to_ppalign(align):
    return {
        'Лево': PP_ALIGN.LEFT,
        'Центр': PP_ALIGN.CENTER,
        'Право': PP_ALIGN.RIGHT,
    }[align]
    

def add_shape_to_slide(slide, coords, text, align):

    coords_list = [ c.strip() for c in coords.split(',')]

    t_left = Cm(float(coords_list[0]))
    t_top = Cm(float(coords_list[1]))
    t_width = Cm(float(coords_list[2]))
    t_height = Cm(float(coords_list[3]))
    txBox = slide.shapes.add_textbox(t_left, t_top, t_width,t_height)
    tf = txBox.text_frame.paragraphs[0]
    tf.vertical_anchor = MSO_ANCHOR.TOP
    tf.word_wrap = False
    tf.margin_top = 0
    tf.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT
    tf.alignment = align_to_ppalign(align)
    run = tf.add_run()
    run.text = f"{text}"
    font = run.font
    font.name = 'Aeroport'
    font.size = Pt(30)
    font.bold = True
    font.italic = None

    return slide


def generate_certificate_pptx(project_home, sheet, cell_row, cell_columns, text_coords, align):
            
    name = clean_cell(sheet.cell(cell_row, int(cell_columns[0])).value)
    school = clean_cell(sheet.cell(cell_row, int(cell_columns[1])).value)
    city = clean_cell(sheet.cell(cell_row, int(cell_columns[2])).value)
    place = clean_cell(sheet.cell(cell_row, int(cell_columns[3])).value)
    
    if place:
        name_place = name + " за " + place
        pptx_pattern = Presentation(os.path.join(project_home, "Шаблон-Диплом.pptx"))
    else:
        name_place = name
        pptx_pattern = Presentation(os.path.join(project_home, "Шаблон-Сертификат.pptx"))

    slide = pptx_pattern.slides[0]

    slide = add_shape_to_slide(slide, text_coords[0], name_place, align)
    slide = add_shape_to_slide(slide, text_coords[1], school, align)
    slide = add_shape_to_slide(slide, text_coords[2], city, align)
    
    # We have to filter some additional symbols from file name.
    # A lot of different quotes here and they keep coming.
    clean_filename = re.sub("[\"\'\”\“\’\‘]", "", name)
    
    if place:
        name_files = os.path.join(project_home, "Дипломы", clean_filename + '.pptx')
    else:
        name_files = os.path.join(project_home, "Сертификаты", clean_filename + '.pptx')

    try:
        pptx_pattern.save(name_files)
        return True, ""
    except:
        return False, name_files