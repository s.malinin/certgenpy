# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/main.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(786, 615)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gbAwardList = QtWidgets.QGroupBox(self.centralwidget)
        self.gbAwardList.setGeometry(QtCore.QRect(10, 10, 351, 171))
        self.gbAwardList.setObjectName("gbAwardList")
        self.btnAwardListOpen = QtWidgets.QPushButton(self.gbAwardList)
        self.btnAwardListOpen.setGeometry(QtCore.QRect(250, 30, 90, 31))
        self.btnAwardListOpen.setObjectName("btnAwardListOpen")
        self.cbAwardListSheet = QtWidgets.QComboBox(self.gbAwardList)
        self.cbAwardListSheet.setEnabled(False)
        self.cbAwardListSheet.setGeometry(QtCore.QRect(110, 30, 121, 31))
        self.cbAwardListSheet.setObjectName("cbAwardListSheet")
        self.lblAwardListState = QtWidgets.QLabel(self.gbAwardList)
        self.lblAwardListState.setGeometry(QtCore.QRect(10, 30, 91, 31))
        self.lblAwardListState.setAlignment(QtCore.Qt.AlignCenter)
        self.lblAwardListState.setObjectName("lblAwardListState")
        self.gbColumns = QtWidgets.QGroupBox(self.gbAwardList)
        self.gbColumns.setGeometry(QtCore.QRect(10, 70, 331, 91))
        self.gbColumns.setObjectName("gbColumns")
        self.edtColumnsName = QtWidgets.QLineEdit(self.gbColumns)
        self.edtColumnsName.setGeometry(QtCore.QRect(30, 60, 31, 25))
        self.edtColumnsName.setAlignment(QtCore.Qt.AlignCenter)
        self.edtColumnsName.setObjectName("edtColumnsName")
        self.lblColumnsName = QtWidgets.QLabel(self.gbColumns)
        self.lblColumnsName.setGeometry(QtCore.QRect(20, 30, 51, 20))
        self.lblColumnsName.setAlignment(QtCore.Qt.AlignCenter)
        self.lblColumnsName.setObjectName("lblColumnsName")
        self.lblColumnsSchool = QtWidgets.QLabel(self.gbColumns)
        self.lblColumnsSchool.setGeometry(QtCore.QRect(90, 30, 71, 20))
        self.lblColumnsSchool.setAlignment(QtCore.Qt.AlignCenter)
        self.lblColumnsSchool.setObjectName("lblColumnsSchool")
        self.lblColumnsCity = QtWidgets.QLabel(self.gbColumns)
        self.lblColumnsCity.setGeometry(QtCore.QRect(170, 30, 71, 20))
        self.lblColumnsCity.setAlignment(QtCore.Qt.AlignCenter)
        self.lblColumnsCity.setObjectName("lblColumnsCity")
        self.lblColumnsPlace = QtWidgets.QLabel(self.gbColumns)
        self.lblColumnsPlace.setGeometry(QtCore.QRect(246, 30, 81, 20))
        self.lblColumnsPlace.setAlignment(QtCore.Qt.AlignCenter)
        self.lblColumnsPlace.setObjectName("lblColumnsPlace")
        self.edtColumnsSchool = QtWidgets.QLineEdit(self.gbColumns)
        self.edtColumnsSchool.setGeometry(QtCore.QRect(110, 60, 31, 25))
        self.edtColumnsSchool.setAlignment(QtCore.Qt.AlignCenter)
        self.edtColumnsSchool.setObjectName("edtColumnsSchool")
        self.edtColumnsCity = QtWidgets.QLineEdit(self.gbColumns)
        self.edtColumnsCity.setGeometry(QtCore.QRect(190, 60, 31, 25))
        self.edtColumnsCity.setAlignment(QtCore.Qt.AlignCenter)
        self.edtColumnsCity.setObjectName("edtColumnsCity")
        self.edtColumnsPlace = QtWidgets.QLineEdit(self.gbColumns)
        self.edtColumnsPlace.setGeometry(QtCore.QRect(270, 60, 31, 25))
        self.edtColumnsPlace.setAlignment(QtCore.Qt.AlignCenter)
        self.edtColumnsPlace.setObjectName("edtColumnsPlace")
        self.gbTemplates = QtWidgets.QGroupBox(self.centralwidget)
        self.gbTemplates.setGeometry(QtCore.QRect(10, 200, 351, 111))
        self.gbTemplates.setObjectName("gbTemplates")
        self.lblTemplateCertTitle = QtWidgets.QLabel(self.gbTemplates)
        self.lblTemplateCertTitle.setGeometry(QtCore.QRect(10, 30, 111, 31))
        self.lblTemplateCertTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTemplateCertTitle.setObjectName("lblTemplateCertTitle")
        self.lblTemplateCertState = QtWidgets.QLabel(self.gbTemplates)
        self.lblTemplateCertState.setGeometry(QtCore.QRect(130, 30, 101, 31))
        self.lblTemplateCertState.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTemplateCertState.setObjectName("lblTemplateCertState")
        self.btnTemplateCertOpen = QtWidgets.QPushButton(self.gbTemplates)
        self.btnTemplateCertOpen.setGeometry(QtCore.QRect(250, 30, 90, 31))
        self.btnTemplateCertOpen.setObjectName("btnTemplateCertOpen")
        self.lblTemplateDplTitle = QtWidgets.QLabel(self.gbTemplates)
        self.lblTemplateDplTitle.setGeometry(QtCore.QRect(10, 70, 111, 31))
        self.lblTemplateDplTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTemplateDplTitle.setObjectName("lblTemplateDplTitle")
        self.lblTemplateDplState = QtWidgets.QLabel(self.gbTemplates)
        self.lblTemplateDplState.setGeometry(QtCore.QRect(130, 70, 101, 31))
        self.lblTemplateDplState.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTemplateDplState.setObjectName("lblTemplateDplState")
        self.btnTemplateDplOpen = QtWidgets.QPushButton(self.gbTemplates)
        self.btnTemplateDplOpen.setGeometry(QtCore.QRect(250, 70, 90, 31))
        self.btnTemplateDplOpen.setObjectName("btnTemplateDplOpen")
        self.gbCoords = QtWidgets.QGroupBox(self.centralwidget)
        self.gbCoords.setGeometry(QtCore.QRect(10, 340, 351, 211))
        self.gbCoords.setObjectName("gbCoords")
        self.lblCoordsHint = QtWidgets.QLabel(self.gbCoords)
        self.lblCoordsHint.setGeometry(QtCore.QRect(70, 30, 281, 21))
        self.lblCoordsHint.setAlignment(QtCore.Qt.AlignCenter)
        self.lblCoordsHint.setObjectName("lblCoordsHint")
        self.lblCoordsNamePlace = QtWidgets.QLabel(self.gbCoords)
        self.lblCoordsNamePlace.setGeometry(QtCore.QRect(0, 60, 121, 21))
        self.lblCoordsNamePlace.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblCoordsNamePlace.setObjectName("lblCoordsNamePlace")
        self.lblCoordsSchool = QtWidgets.QLabel(self.gbCoords)
        self.lblCoordsSchool.setGeometry(QtCore.QRect(60, 100, 61, 21))
        self.lblCoordsSchool.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblCoordsSchool.setObjectName("lblCoordsSchool")
        self.lblCoordsCity = QtWidgets.QLabel(self.gbCoords)
        self.lblCoordsCity.setGeometry(QtCore.QRect(60, 140, 61, 21))
        self.lblCoordsCity.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblCoordsCity.setObjectName("lblCoordsCity")
        self.edtCoordsNamePlace = QtWidgets.QLineEdit(self.gbCoords)
        self.edtCoordsNamePlace.setGeometry(QtCore.QRect(130, 60, 211, 25))
        self.edtCoordsNamePlace.setObjectName("edtCoordsNamePlace")
        self.edtCoordsSchool = QtWidgets.QLineEdit(self.gbCoords)
        self.edtCoordsSchool.setGeometry(QtCore.QRect(130, 100, 211, 25))
        self.edtCoordsSchool.setObjectName("edtCoordsSchool")
        self.edtCoordsCity = QtWidgets.QLineEdit(self.gbCoords)
        self.edtCoordsCity.setGeometry(QtCore.QRect(130, 140, 211, 25))
        self.edtCoordsCity.setObjectName("edtCoordsCity")
        self.lblTextAlign = QtWidgets.QLabel(self.gbCoords)
        self.lblTextAlign.setGeometry(QtCore.QRect(40, 180, 131, 21))
        self.lblTextAlign.setObjectName("lblTextAlign")
        self.cbTextAlign = QtWidgets.QComboBox(self.gbCoords)
        self.cbTextAlign.setGeometry(QtCore.QRect(190, 174, 101, 31))
        self.cbTextAlign.setObjectName("cbTextAlign")
        self.cbTextAlign.addItem("")
        self.cbTextAlign.addItem("")
        self.cbTextAlign.addItem("")
        self.pbCreate = QtWidgets.QProgressBar(self.centralwidget)
        self.pbCreate.setEnabled(False)
        self.pbCreate.setGeometry(QtCore.QRect(400, 120, 361, 21))
        self.pbCreate.setProperty("value", 0)
        self.pbCreate.setAlignment(QtCore.Qt.AlignCenter)
        self.pbCreate.setTextVisible(True)
        self.pbCreate.setObjectName("pbCreate")
        self.lblLog = QtWidgets.QLabel(self.centralwidget)
        self.lblLog.setGeometry(QtCore.QRect(510, 160, 131, 20))
        self.lblLog.setAlignment(QtCore.Qt.AlignCenter)
        self.lblLog.setObjectName("lblLog")
        self.tedtLog = QtWidgets.QTextEdit(self.centralwidget)
        self.tedtLog.setGeometry(QtCore.QRect(400, 200, 361, 351))
        self.tedtLog.setObjectName("tedtLog")
        self.btnCreate = QtWidgets.QPushButton(self.centralwidget)
        self.btnCreate.setEnabled(False)
        self.btnCreate.setGeometry(QtCore.QRect(460, 60, 231, 41))
        self.btnCreate.setObjectName("btnCreate")
        self.lblGenRange = QtWidgets.QLabel(self.centralwidget)
        self.lblGenRange.setGeometry(QtCore.QRect(400, 20, 141, 21))
        self.lblGenRange.setAlignment(QtCore.Qt.AlignCenter)
        self.lblGenRange.setObjectName("lblGenRange")
        self.edtGenRangeLow = QtWidgets.QLineEdit(self.centralwidget)
        self.edtGenRangeLow.setGeometry(QtCore.QRect(550, 20, 61, 25))
        self.edtGenRangeLow.setAlignment(QtCore.Qt.AlignCenter)
        self.edtGenRangeLow.setObjectName("edtGenRangeLow")
        self.edtGenRangeHigh = QtWidgets.QLineEdit(self.centralwidget)
        self.edtGenRangeHigh.setGeometry(QtCore.QRect(630, 20, 61, 25))
        self.edtGenRangeHigh.setAlignment(QtCore.Qt.AlignCenter)
        self.edtGenRangeHigh.setObjectName("edtGenRangeHigh")
        self.lblGenDash = QtWidgets.QLabel(self.centralwidget)
        self.lblGenDash.setGeometry(QtCore.QRect(610, 20, 21, 21))
        self.lblGenDash.setAlignment(QtCore.Qt.AlignCenter)
        self.lblGenDash.setObjectName("lblGenDash")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 786, 22))
        self.menubar.setObjectName("menubar")
        self.menuMain = QtWidgets.QMenu(self.menubar)
        self.menuMain.setObjectName("menuMain")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actMenuMainNewProject = QtWidgets.QAction(MainWindow)
        self.actMenuMainNewProject.setObjectName("actMenuMainNewProject")
        self.actMenuMainOpenProject = QtWidgets.QAction(MainWindow)
        self.actMenuMainOpenProject.setObjectName("actMenuMainOpenProject")
        self.actMenuMainSaveProject = QtWidgets.QAction(MainWindow)
        self.actMenuMainSaveProject.setEnabled(False)
        self.actMenuMainSaveProject.setObjectName("actMenuMainSaveProject")
        self.actMenuMainExit = QtWidgets.QAction(MainWindow)
        self.actMenuMainExit.setObjectName("actMenuMainExit")
        self.actMenuMainSaveAs = QtWidgets.QAction(MainWindow)
        self.actMenuMainSaveAs.setObjectName("actMenuMainSaveAs")
        self.menuMain.addAction(self.actMenuMainNewProject)
        self.menuMain.addAction(self.actMenuMainOpenProject)
        self.menuMain.addAction(self.actMenuMainSaveProject)
        self.menuMain.addAction(self.actMenuMainSaveAs)
        self.menuMain.addSeparator()
        self.menuMain.addAction(self.actMenuMainExit)
        self.menubar.addAction(self.menuMain.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Генератор сертификатов"))
        self.gbAwardList.setTitle(_translate("MainWindow", "Список награждаемых"))
        self.btnAwardListOpen.setText(_translate("MainWindow", "Открыть"))
        self.lblAwardListState.setText(_translate("MainWindow", "Не выбран"))
        self.gbColumns.setTitle(_translate("MainWindow", "Номера столбцов"))
        self.edtColumnsName.setText(_translate("MainWindow", "1"))
        self.lblColumnsName.setText(_translate("MainWindow", "Имя"))
        self.lblColumnsSchool.setText(_translate("MainWindow", "Школа"))
        self.lblColumnsCity.setText(_translate("MainWindow", "Город"))
        self.lblColumnsPlace.setText(_translate("MainWindow", "Место"))
        self.edtColumnsSchool.setText(_translate("MainWindow", "2"))
        self.edtColumnsCity.setText(_translate("MainWindow", "3"))
        self.edtColumnsPlace.setText(_translate("MainWindow", "4"))
        self.gbTemplates.setTitle(_translate("MainWindow", "Шаблоны"))
        self.lblTemplateCertTitle.setText(_translate("MainWindow", "Сертификат"))
        self.lblTemplateCertState.setText(_translate("MainWindow", "Не выбран"))
        self.btnTemplateCertOpen.setText(_translate("MainWindow", "Открыть"))
        self.lblTemplateDplTitle.setText(_translate("MainWindow", "Диплом"))
        self.lblTemplateDplState.setText(_translate("MainWindow", "Не выбран"))
        self.btnTemplateDplOpen.setText(_translate("MainWindow", "Открыть"))
        self.gbCoords.setTitle(_translate("MainWindow", "Координаты текста"))
        self.lblCoordsHint.setText(_translate("MainWindow", "Лево, Верх, Ширина, Высота (см)"))
        self.lblCoordsNamePlace.setText(_translate("MainWindow", "Имя и место"))
        self.lblCoordsSchool.setText(_translate("MainWindow", "Школа"))
        self.lblCoordsCity.setText(_translate("MainWindow", "Город"))
        self.edtCoordsNamePlace.setText(_translate("MainWindow", "3.7, 11, 21, 1.5"))
        self.edtCoordsSchool.setText(_translate("MainWindow", "3.7, 14, 21, 1.5"))
        self.edtCoordsCity.setText(_translate("MainWindow", "3.7, 15.5, 21, 1.5"))
        self.lblTextAlign.setText(_translate("MainWindow", "Выравнивание"))
        self.cbTextAlign.setCurrentText(_translate("MainWindow", "Центр"))
        self.cbTextAlign.setItemText(0, _translate("MainWindow", "Лево"))
        self.cbTextAlign.setItemText(1, _translate("MainWindow", "Центр"))
        self.cbTextAlign.setItemText(2, _translate("MainWindow", "Право"))
        self.lblLog.setText(_translate("MainWindow", "Сообщения"))
        self.btnCreate.setText(_translate("MainWindow", "Сгенерировать"))
        self.lblGenRange.setText(_translate("MainWindow", "Диапазон строк"))
        self.lblGenDash.setText(_translate("MainWindow", "-"))
        self.menuMain.setTitle(_translate("MainWindow", "Меню"))
        self.actMenuMainNewProject.setText(_translate("MainWindow", "Новый проект"))
        self.actMenuMainNewProject.setShortcut(_translate("MainWindow", "Ctrl+N"))
        self.actMenuMainOpenProject.setText(_translate("MainWindow", "Открыть проект"))
        self.actMenuMainOpenProject.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actMenuMainSaveProject.setText(_translate("MainWindow", "Сохранить проект"))
        self.actMenuMainSaveProject.setShortcut(_translate("MainWindow", "Ctrl+S"))
        self.actMenuMainExit.setText(_translate("MainWindow", "Выход"))
        self.actMenuMainExit.setShortcut(_translate("MainWindow", "Ctrl+X"))
        self.actMenuMainSaveAs.setText(_translate("MainWindow", "Сохранить как ..."))
